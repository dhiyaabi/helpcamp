var express = require('express');

var app = express();
var bodyParser = require('body-parser')
var mongoose = require("mongoose");
mongoose.connect("mongodb://admin:admin1*@ds135441.mlab.com:35441/helpcamp");


//mongo schema

var campgroundSchema = new mongoose.Schema ({
	name: String,
	image: String
	description: String
});

var campground =mongoose.model("campground", campgroundSchema);

//campground.create({

	//name:"mt maunganui" ,
	//image:"https://farm6.staticflickr.com/5080/7004537596_a29365d72f.jpg"

//},function(err,campground){
	//if(err){
		//console.log(err);
	//}else{
		//console.log("newly created campground");
		//console.log(campground);
	//}

//}
//)


app.use(bodyParser.urlencoded({extended:true}));

app.set('view engine', 'ejs');

	//var campgrounds=[
	//{name:"salmon creek" ,image:"https://farm2.staticflickr.com/1281/4684194306_18ebcdb01c.jpg"},
	//{name:"mt maunganui" ,image:"https://farm6.staticflickr.com/5080/7004537596_a29365d72f.jpg"},
	//{name:"granite hills" ,image:"https://farm3.staticflickr.com/2238/1514148183_092606ba94.jpg"},
//];

app.get('/', function(req, res){
	res.render('landing');
})

app.get("/campgrounds",function(req,res){

	campground.find({},function(err,allCampgrounds){
		if(err){
			console.log(error)
		}else{
             res.render("campgrounds",{campgrounds:allCampgrounds});
		}
	})
  
})

app.post("/campgrounds",function(req,res){
	//res.send("you hit the post route")

	var name = req.body.name;
	var image = req.body.image;
	var newCampground ={name:name, image:image}
	//campgrounds.push(newCampground)
	campground.create(newCampground,function(err,newlycreate){
		if(err){
			console.log(err);
		}else{
			res.redirect("/campgrounds");
		}

	});
	
})

app.get("/campgrounds/new",function(req,res){
	res.render("new.ejs")
})

app.get("/campgrounds/:id",function(req,res){
	res.send("this will be the new show page")
})
app.listen(3000,function(){
	console.log('Helpcamp Server Started!')
})